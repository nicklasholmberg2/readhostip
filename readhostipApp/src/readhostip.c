// C program to display hostname
// and IP address
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <aSubRecord.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <limits.h>
  

void remove_dots(char* restrict str_trimmed, const char* restrict str_untrimmed)
{
    int dots=0;
    while (*str_untrimmed != '\0')
    {
        if('.'!=(*str_untrimmed) || dots == 1)
        {
            *str_trimmed = *str_untrimmed;
            str_trimmed++;
        }
        if('.'==(*str_untrimmed))
        {
            dots++;
        }
        str_untrimmed++;
    }
    *str_trimmed = '\0';
}


int getIpAddress(char ethmod[256], char currentIP[16]) {
    struct ifreq ifr;
    int sock = 0;
    strcpy(currentIP,"0.0.0.0");
    strcpy(ifr.ifr_name,"default"); 
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    strcpy(ifr.ifr_name, ethmod);
    ioctl(sock, SIOCGIFADDR, &ifr);
    strcpy(currentIP, inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr));
    close(sock);
    if(strcmp(currentIP,"0.0.0.0")){
        return 1; 
    }
    return 0;
}

int getHostName(char hostname[HOST_NAME_MAX + 1]){
    char hostname1[HOST_NAME_MAX + 1];

    hostname1[HOST_NAME_MAX] = 0;
    if (gethostname(hostname1, HOST_NAME_MAX) == 0){
        strcpy(hostname,hostname1);
        return 0;
    } else {
        strcpy(hostname,"error");
        perror("gethostname");
        return 1;
    }
}


// Driver code
static int ReadHostIP(aSubRecord *precord)
{
    char ethmodIP[16];
    char hostname[HOST_NAME_MAX+1];
    double prevIP;
    char ethmod[256];
    char ipstr[256];
    double ipreal=0.0;
    
    strcpy(ethmod, precord->a); 
    
    // get hostname
    getHostName(hostname);
  
    // Save previous IP
    prevIP = *(double *)precord->b;
    // Read current IP
    getIpAddress(ethmod, ethmodIP);
    // Convert to string and boolean
    remove_dots(ipstr, ethmodIP);
    ipreal = atof(ipstr);
  
    // Outputs
    strcpy(precord->vala, hostname);
    strcpy(precord->valb, ethmodIP);
    strcpy(precord->valc, ipstr);
    *(double *)precord->vald = ipreal;
    *(long *)precord->vale = (ipreal != prevIP);

    printf("Hostname: %s\n", hostname);
    printf("Host IP: %s\n", ethmodIP);
    printf("ipstr: %s\n", ipstr);
    printf("float: %lf\n", ipreal);
    printf("previp: %lf\n", prevIP);
    printf("New IP: %d\n",(ipreal != prevIP));

    return 0;
}
epicsRegisterFunction(ReadHostIP);
