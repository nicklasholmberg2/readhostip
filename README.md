readhostip
======
European Spallation Source ERIC Site-specific EPICS module : readhostip


This module should compile as a regular EPICS module.

* Run `make` from the command line.

This module should also compile as an E3 module:

* Install `conda` (https://docs.conda.io/en/latest/miniconda.html)
* Create and activate a conda environment that minimally includes the following packages: `make perl tclx compilers epics-base require`
* Run `make -f Makefile.E3 target` to build `target` using the E3 build process

Alternative -1: https://www.geeksforgeeks.org/c-program-display-hostname-ip-address/
This is implemented in https://gitlab.esss.lu.se/nicklasholmberg2/readhostip/-/blob/master/readhostipApp/src/readhostip.c

Alternative 0:
https://stackoverflow.com/questions/2865583/gethostbyname-in-c
```c
#include <stdio.h>
#include <unistd.h>
#include <limits.h>

int main(int argc, char *argv[])
{
    char hostname[HOST_NAME_MAX + 1];

    hostname[HOST_NAME_MAX] = 0;
    if (gethostname(hostname, HOST_NAME_MAX) == 0)
        puts(hostname);
    else
        perror("gethostname");

    return 0;
}
```

Alternative 1: https://forgetcode.com/C/1483-Program-to-Get-the-IP-Address
```c
#include <ifaddrs.h>
#include <stdio.h>
int main()
{
struct ifaddrs *id;
int val;
val = getifaddrs(&id);
printf("Network Interface Name :- %s\n",id->ifa_name);
printf("Network Address of %s :- %d\n",id->ifa_name,id->ifa_addr);
printf("Network Data :- %d \n",id->ifa_data);
printf("Socket Data : -%c\n",id->ifa_addr->sa_data);
return 0;
}
```

Alternative 2: https://www.includehelp.com/c-programs/get-ip-address-in-linux.aspx 
```c
/*C program to get IP Address of Linux Computer System.*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

int main()
{
    unsigned char ip_address[15];
    int fd;
    struct ifreq ifr;

    /*AF_INET - to define network interface IPv4*/
    /*Creating soket for it.*/
    fd = socket(AF_INET, SOCK_DGRAM, 0);

    /*AF_INET - to define IPv4 Address type.*/
    ifr.ifr_addr.sa_family = AF_INET;

    /*eth0 - define the ifr_name - port name
    where network attached.*/
    memcpy(ifr.ifr_name, "eth0", IFNAMSIZ - 1);

    /*Accessing network interface information by
    passing address using ioctl.*/
    ioctl(fd, SIOCGIFADDR, &ifr);
    /*closing fd*/
    close(fd);

    /*Extract IP Address*/
    strcpy(ip_address, inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr));

    printf("System IP Address is: %s\n", ip_address);

    return 0;
}
```

Alternative 3: https://www.cnx-software.com/2011/04/05/c-code-to-get-mac-address-and-ip-address/
```c

/* Returns the MAC Address
   Params: int iNetType - 0: ethernet, 1: Wifi
           char chMAC[6] - MAC Address in binary format
   Returns: 0: success
           -1: Failure
    */
int getMACAddress(int iNetType, char chMAC[6]) {
  struct ifreq ifr;
  int sock;
  char *ifname=NULL;
 
  if (!iNetType) {
    ifname="eth0"; /* Ethernet */
  } else {
    ifname="wlan0"; /* Wifi */
  }
  sock=socket(AF_INET,SOCK_DGRAM,0);
  strcpy( ifr.ifr_name, ifname );
  ifr.ifr_addr.sa_family = AF_INET;
  if (ioctl( sock, SIOCGIFHWADDR, &amp;ifr ) &lt; 0) {
    return -1;
  }
  memcpy(chMAC, ifr.ifr_hwaddr.sa_data, 6)
  close(sock);
  return 0;
}
```
Function in C to return the IP Address:
```c
/* Returns the interface IP Address
   Params: int iNetType - 0: ethernet, 1: Wifi
           char *chIP - IP Address string
   Return: 0: success / -1: Failure
    */
int getIpAddress(int iNetType, char chIP[16]) {
  struct ifreq ifr;
  int sock = 0;
 
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if(iNetType == 0) {
    strcpy(ifr.ifr_name, "eth0");
  } else {
    strcpy(ifr.ifr_name, "wlan0");
  }
  if (ioctl(sock, SIOCGIFADDR, &amp;ifr) &lt; 0) {
    strcpy(chIP, "0.0.0.0");
    return -1;
 }
 sprintf(chIP, "%s", inet_ntoa(((struct sockaddr_in *) &amp;(ifr.ifr_addr))-&gt;sin_addr));
 close(sock);
 return 0;
}
```

alternative 4: https://www.man7.org/linux/man-pages/man3/getaddrinfo.3.html
